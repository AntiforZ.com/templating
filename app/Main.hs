module Main where

import qualified Data.ByteString.Lazy.Char8 as C
import Footer
import Header
import Menu
import Pages
import Gallery
import GigGallery
import Gigs
import Photos
import AudioGallery
import Music
import Text.Blaze.Renderer.Utf8 (renderMarkup)

main :: IO ()
main = do
  writeFile "../AntiforZ-site/site/templates/header.html" $
    C.unpack . renderMarkup $ headerHtml
  writeFile "../AntiforZ-site/site/templates/menu.html" $
    C.unpack . renderMarkup $ menuHtml
  writeFile "../AntiforZ-site/site/templates/footer.html" $
    C.unpack . renderMarkup $ footerHtml
  writeFile "../AntiforZ-site/site/templates/gallery.html" $
    C.unpack . renderMarkup $ galleryHtml
  writeFile "../AntiforZ-site/site/templates/photos.html" $
    C.unpack . renderMarkup $ photosHtml
  writeFile "../AntiforZ-site/site/templates/gigGallery.html" $
    C.unpack . renderMarkup $ gigGalleryHtml
  writeFile "../AntiforZ-site/site/templates/gigs.html" $
    C.unpack . renderMarkup $ gigsHtml
  writeFile "../AntiforZ-site/site/templates/pages.html" $
    C.unpack . renderMarkup $ pagesHtml
  writeFile "../AntiforZ-site/site/templates/music.html" $
    C.unpack . renderMarkup $ musicHtml
  writeFile "../AntiforZ-site/site/templates/audioGallery.html" $
    C.unpack . renderMarkup $ audioGalleryHtml

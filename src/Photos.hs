
module Photos (photosHtml) where


import Data.Monoid
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import Control.Monad(forM_)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

photosHtml :: Html
photosHtml = do
  preEscapedString "{{>templates/header.html}}" 
  preEscapedString "{{>templates/menu.html}}" 
  H.div ! A.class_ "txt" $ do
    h1 " Photos"
    H.br
    p " Since all of us are exceptionally handsome, and even more so our audiences it makes absolute sense to come and see the pictures. Enjoy!"
  H.br
  preEscapedString "{{#galleries}}"
  preEscapedString "{{>templates/gallery.html}}" 
  preEscapedString "{{/galleries}}"
  preEscapedString "{{>templates/footer.html}}" 

module About
  ( aboutHtml
  ) where

import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

aboutHtml :: Html
aboutHtml = do
  preEscapedString "{{>templates/header.html}}"
  preEscapedString "{{>templates/menu.html}}"
  preEscapedString "{{{content}}}"
  preEscapedString "{{>templates/footer.html}}"

module Header
  ( headerHtml
  ) where

import Control.Monad (forM_)
import Data.Monoid
import Data.Text (pack, toLower)
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

headerHtml :: Html
headerHtml =
  docTypeHtml $ do
    H.head $ do
      meta ! A.charset "UTF-8"
      link ! A.rel "stylesheet" ! A.href "/css/default.css"
      link ! A.rel "stylesheet" ! A.href "/css/photos.css"
      link ! A.rel "stylesheet" ! A.href "/css/video.css"
      link ! A.rel "stylesheet" ! A.href "/css/gigs.css"
      link ! A.rel "stylesheet" ! A.href "/css/audio.css"
      meta ! customAttribute "http-equiv" "x-ua-compatible" !
        A.content "ie=edge"
      meta ! A.name "viewport" ! A.content "width=device-width, initial-scale=1"

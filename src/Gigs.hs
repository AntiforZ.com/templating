module Gigs (gigsHtml) where


import Data.Monoid
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

gigsHtml :: Html
gigsHtml = do
  preEscapedString "{{>templates/header.html}}" 
  preEscapedString "{{>templates/menu.html}}" 
  H.div ! A.class_ "txt" $ do
    h1 " Gigs"
    H.br
    p " Yes, you have entered that VERY SPECIAL place! This is where the magic happens! Come join us for a drink and doubler-left-foot choreography at :" 
  H.br
  H.img ! A.class_ "next_gig txt" ! A.src "/gigs/next_gig.jpg"
  H.div ! A.class_ "txt" $ do
    preEscapedString "{{#galleries}}"
    preEscapedString "{{>templates/gigGallery.html}}" 
  H.div ! A.class_ "txt" $ do
    preEscapedString "{{/galleries}}"
    preEscapedString "{{>templates/footer.html}}" 

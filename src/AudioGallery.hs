
module AudioGallery (audioGalleryHtml) where

import Data.Monoid
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import Control.Monad(forM_)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

audioGalleryHtml :: Html
audioGalleryHtml = do
  H.div ! A.class_ "audio_wrap txt" $ do
    H.div ! A.class_ "audioplayer" $ do
      preEscapedString "<iframe src=\"https://open.spotify.com/follow/1/?uri=spotify:artist:2xb9wqp3kd8zFDcyOVYXJK?si=jLlq8xA_TCS54qd38CsMeg&size=detail&theme=light\" width=\"300\" height=\"56\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden;\" allowtransparency=\"true\"></iframe>"
      h2 "{{name}}"
      preEscapedString "{{#songs}}"
      p (preEscapedString "{{src}}")
      H.audio ! A.controls "controls" ! A.class_ "audio_control"  $ do
        H.source ! A.src "{{{url}}}" ! A.type_ "audio/mpeg" 
      preEscapedString "{{/songs}}"

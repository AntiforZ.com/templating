module Menu
  ( menuHtml
  ) where

import Control.Monad (forM_)
import Data.Monoid
import Data.Text (pack, toLower)
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

menuHtml :: Html
menuHtml = do
  H.header $ do
    label ! A.for "show-menu" ! A.class_ "show-menu" $ "Show Menu"
    input ! A.type_ "checkbox" ! A.id "show-menu" !
      customAttribute "role" "button"
    ul ! A.id "menu" ! A.class_ "menu" $ mapM_ nest menuItems
    H.div ! A.class_ "book_us" $ do
      a ! A.href "https://www.gigstarter.nl/artists/antiforz" ! A.target "_blank" ! A.rel "noopener noreferrer" $ do
        img ! A.src "https://gigstarter.s3.amazonaws.com/book_us.png" ! A.alt "Book AntiforZ" ! A.width "110px"
    img ! A.src "/images/AntiforZ-banner.png" ! A.class_ "header-banner"

nest :: (String, [String]) -> Html
nest (levelOne, levelTwo) = do
  li ! A.class_ "menu" $ do
    nest1 (levelOne, levelTwo)
    nest2 levelTwo
  where
    nest1 (levelOne, []) = a ! A.href (linkify levelOne) $ (toHtml levelOne)
    nest1 (_, _) = a ! A.href "#" $ (toHtml levelOne)
    nest2 [] = ""
    nest2 x =
      ul ! A.class_ "hidden" $
      forM_ x $ (\y -> li $ do a ! A.href (linkify y) $ (toHtml y))

linkify :: String -> AttributeValue
linkify "Home" = H.toValue . pack $ "/"
linkify link =
  H.toValue . toLower . pack . filter (/= ' ') $ "/" ++ link ++ ".html"

menuItems :: [(String, [String])]
menuItems =
  [ ("Home", [])
  , ("About", [])
  , ("Gigs", [])
  , ("Portfolio", ["Photos", "Music", "Videos"])
  , ("Get In Touch", [])
  ]

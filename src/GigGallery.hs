module GigGallery (gigGalleryHtml) where

import Data.Monoid
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

gigGalleryHtml :: Html
gigGalleryHtml = do
  H.table $ do
    h2 (preEscapedString "{{name}}")
    preEscapedString "{{#images}}"
    H.tr $ do
      H.td ! A.class_ "gigBox" $ do
        H.div $ do
          H.div ! A.class_ "gigBoxInner" $ do
            H.a ! A.href "{{{url}}}" $ do
              H.img ! A.src "{{{url}}}"
      H.td "{{src}}"
    preEscapedString "{{/images}}"

module Index (indexHtml) where

import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

indexHtml :: Html
indexHtml = do
  preEscapedString "{{>templates/header.html}}"
  preEscapedString "{{>templates/menu.html}}"
  H.div ! A.class_ "txt" $ do
    preEscapedString "{{{content}}}"
  preEscapedString "{{>templates/footer.html}}"

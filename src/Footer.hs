module Footer
  ( footerHtml
  ) where

import Control.Monad (forM_)
import Data.Monoid
import Data.Text (pack, toLower)
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

footerHtml :: Html
footerHtml =
  docTypeHtml $ do
    H.footer $ do
      H.div ! A.class_ "footer" $ do
        H.br
        H.br
        H.br
        "Site grooves and quirks by "
        a ! A.href "https://gitlab.com/ProofOfPizza" $ "ProofOfPizza"
        " with the extraterrestrial powers of "
        a ! A.href "https://github.com/ChrisPenner/SitePipe#quick-start" $
          "SitePipe"

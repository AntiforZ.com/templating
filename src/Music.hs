
module Music (musicHtml) where

import Data.Monoid
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import Control.Monad(forM_)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

musicHtml :: Html
musicHtml = do
  preEscapedString "{{>templates/header.html}}" 
  preEscapedString "{{>templates/menu.html}}" 
  H.div ! A.class_ "txt" $ do
    h1 " Music Maestro!"
    H.br
    p " This is what it's all about: Melodies to melt your mind and groovy beats to the butt!"
  H.br
  preEscapedString "{{#galleries}}"
  preEscapedString "{{>templates/audioGallery.html}}" 
  preEscapedString "{{/galleries}}"
  preEscapedString "{{>templates/footer.html}}" 

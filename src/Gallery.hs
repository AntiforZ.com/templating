module Gallery (galleryHtml) where

import Data.Monoid
import Text.Blaze (ToMarkup(..))
import Text.Blaze.Html5 hiding (html, param)
import Control.Monad(forM_)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

galleryHtml :: Html
galleryHtml = do
  H.div ! A.class_ "wrap txt" $ do
    h2 (preEscapedString "{{name}}")
    preEscapedString "{{#images}}"
    H.div ! A.class_ "box" $ do
      H.div ! A.class_ "boxInner" $ do
        a ! A.href "{{{url}}}" $ do
          H.img ! A.src "{{{url}}}"
    preEscapedString "{{/images}}"

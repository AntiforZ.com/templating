# Templating

Used for making the moustache templates for AntiforZ.com.

__install__ :

hpack

stack build

stack exec Templating


Note: it will write the templates directly to the site/templates folder of project AntiforZ-site to be used there.


